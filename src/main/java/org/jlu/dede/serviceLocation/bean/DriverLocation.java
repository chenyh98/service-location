package org.jlu.dede.serviceLocation.bean;

import com.mysql.cj.x.protobuf.MysqlxDatatypes;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = " DriverLocation",indexes = { @Index(name="idIndex", columnList="id"),
        @Index(name="geoCodeIndex", columnList="geoCode")
})
public class DriverLocation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String x;
    private String y;
    private String geoCode;
}
