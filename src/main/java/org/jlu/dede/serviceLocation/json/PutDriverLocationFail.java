package org.jlu.dede.serviceLocation.json;

import org.jlu.dede.publicUtlis.json.RestException;

public class PutDriverLocationFail extends RestException {
    public  PutDriverLocationFail(String message)
    {

        super(776,message);
        setErrorCode(Integer.valueOf(776));
    }
}