package org.jlu.dede.serviceLocation.json;

import org.jlu.dede.publicUtlis.json.RestException;

public class NoOrderException extends RestException {
    public NoOrderException(String message) {
        super(773, message);
        setErrorCode(Integer.valueOf(773));
    }
}
