package org.jlu.dede.serviceLocation.json;

import org.jlu.dede.publicUtlis.json.RestException;

public class NoDriverLocationException extends RestException {
    public NoDriverLocationException(String message) {
        super(772, message);
        setErrorCode(Integer.valueOf(772));
    }
}