package org.jlu.dede.serviceLocation.json;

import org.jlu.dede.publicUtlis.json.RestException;

public class PushMessageFail extends RestException{
    public  PushMessageFail(String message)
        {

            super(775,message);
            setErrorCode(Integer.valueOf(775));
        }
}
