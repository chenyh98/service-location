package org.jlu.dede.serviceLocation.json;

import org.jlu.dede.publicUtlis.json.RestException;

public class NoDriverException extends RestException {
    public  NoDriverException(String message)
    {

        super(771,message);
        setErrorCode(Integer.valueOf(771));
    }
}
