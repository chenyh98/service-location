package org.jlu.dede.serviceLocation.json;

import org.jlu.dede.publicUtlis.json.RestException;

public class NoPassengerException extends RestException {
    public  NoPassengerException(String message)
    {
        super(774,message);
        setErrorCode(Integer.valueOf(774));
    }
}