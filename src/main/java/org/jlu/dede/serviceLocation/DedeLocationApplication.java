package org.jlu.dede.serviceLocation;

import com.spatial4j.core.io.GeohashUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients       //开启Feign的功能
public class DedeLocationApplication {

    public static void main(String[] args) {
      //  System.out.println(GeohashUtils.encodeLatLon(Double.valueOf(15.0001),Double.valueOf(13.0)));
        SpringApplication.run(DedeLocationApplication.class, args);
    }

}
