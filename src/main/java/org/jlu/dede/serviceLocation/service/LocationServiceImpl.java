package org.jlu.dede.serviceLocation.service;

import com.spatial4j.core.io.GeohashUtils;

import org.jlu.dede.serviceLocation.bean.DriverLocation;
import org.jlu.dede.serviceLocation.feign.dataaccesslocation.DriverDataAccess;
import org.jlu.dede.serviceLocation.feign.dataaccesslocation.OrderDataAccess;
import org.jlu.dede.serviceLocation.feign.dataaccesslocation.PassengerDataAccess;
import org.jlu.dede.serviceLocation.feign.pushLocation.PushDistance;

import org.jlu.dede.serviceLocation.json.*;
import org.jlu.dede.serviceLocation.repository.LocationDao;
import org.jlu.dede.publicUtlis.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
//@Repository
@Transactional
public class LocationServiceImpl implements LocationService {
    @Autowired
    private DriverDataAccess driverDataAccess;
    @Autowired
    private PassengerDataAccess passengerDataAccess;
    @Autowired
    private OrderDataAccess orderDataAccess;
    @Autowired
    private PushDistance pushDistance;
    @Autowired
    private LocationDao locationDao;
    //保存乘客地理位置
    public boolean storePassengerLocation(String latitude, String longitude, Integer id) {
        Passenger passenger = passengerDataAccess.getByID(id);//可加异常
        try {
            passenger.setX(latitude);
            passenger.setY(longitude);
            passengerDataAccess.updatePassenger(passenger);
        } catch (Exception e) {
             return false;
        }
        return true;
    }

    // 保存司机地理位置
    public boolean storeDriverLocation(String latitude, String longitude, Integer id) {
        Integer integer=new Integer(0);
        if(integer.equals(id))
        {
          //  DriverLocation driverLocation=new DriverLocation();
            id=Integer.valueOf(1+(int)locationDao.count());
            DriverLocation driverLocation=new DriverLocation();
            driverLocation.setId(id);
            driverLocation.setX(latitude);
            driverLocation.setY(longitude);
        //    locationDao.save(driverLocation);
            if(locationDao.save(driverLocation)==null)
            {
                throw new PutDriverLocationFail("Put a DriverLocationXY Fail in service-serviceLocation");
            }
        }
        else
        {
            if(driverDataAccess.getByID(id)==null)
                throw new NoDriverException("no this driver");
            DriverLocation driverLocation=new DriverLocation();
            driverLocation.setId(id);
            driverLocation.setX(latitude);
            driverLocation.setY(longitude);
            String geo_code=GeohashUtils.encodeLatLon(Double.valueOf(latitude),Double.valueOf(longitude));//司机实时位置转geoHash
            driverLocation.setGeoCode(geo_code);

            if(locationDao.save(driverLocation)==null)
            {
                throw new PutDriverLocationFail("Put a DriverLocationXY Fail in service-serviceLocation");
             }
        }
        return true;
    }

    //计算两次间隔之间的实际里程并推送给司机和乘客
    public boolean calculateDistance(String latitude, String longitude, Integer id) {
        Driver driver=orderDataAccess.findDriverByOrder(id);
        Passenger passenger=orderDataAccess.findPassengerByOrder(id);
        System.out.println(id);
        System.out.println(passenger.getId());
        Order order=orderDataAccess.getByID(id);
        if(passenger==null)
            throw new NoPassengerException("PassengerNotFound in service-serviceLocation");
        if(driver==null)
            throw new NoDriverException("DriverNotFound in service-serviceLocation");
        DriverLocation driverLocation=locationDao.findById(driver.getId());
        if(driverLocation==null)
           throw new NoDriverLocationException("NoDriverLocation in location repository");
        String lat=driverLocation.getX();
        String lon=driverLocation.getY();
        double s=GetDistance(Double.valueOf(latitude),Double.valueOf(longitude),Double.valueOf(lat),Double.valueOf(lon));
        double all=s+order.getDistance();
        order.setDistance(all);
        orderDataAccess.updateOrder(id,order);
        String pAlias="p"+passenger.getId();
        String dAlias="d"+driver.getId();
        List<String> list=new LinkedList<String>();
        list.add(pAlias);
        list.add(dAlias);
        Object object=Double.valueOf(all);
        Push push = new Push(list,"","Double",object);
        if(pushDistance.pushAll(push)==false)
        {
            return false;
           // throw new PutDriverLocationFail("pushDistance Fail");
        }

//         Push push1 = new Push(dAlias,"","Double",Double.valueOf(all));
//
//        if(pushDistance.pushOneMessage(push1)==false)
//        {
//            throw new PutDriverLocationFail("pushDistance Fail");
//            //return false;
//        }
       return true;
    }

    //根据两点的经纬度计算两地距离(单位：米)
    private static double rad(double d) {
        return d * Math.PI / 180.0;
    }

    //lon经度
    //lat纬度
    public  double GetDistance(double lon1, double lat1, double lon2, double lat2) {
        double radLat1 = rad(lat1);
        double radLat2 = rad(lat2);
        double a = radLat1 - radLat2;
        double b = rad(lon1) - rad(lon2);
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) +
                Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
        s = s * 6378137.00;//椭球长半轴6378137.0
        s = Math.round(s * 10000) / 10000;
        return s;
    }

    //寻找要推送的司机
    public List<String> findDriver(String x, String y, Integer id){
        Order order=orderDataAccess.getByID(id);
        if(order==null)
            throw new NoOrderException("orderNotFound in service-serviceLocation");
        String passenerGeoCode=GeohashUtils.encodeLatLon(Double.valueOf(x),Double.valueOf(y),5);
        List<Integer> driverList=locationDao.findByGeoCode(passenerGeoCode);
        Passenger passenger=orderDataAccess.findPassengerByOrder(id);
        List<BlackList> blackList=passengerDataAccess.getByPassenger(passenger.getId());//得到黑名单
        for(Integer i:driverList)         //通过黑名单过滤要推送的司机
           for(BlackList b:blackList)
           {
              if(orderDataAccess.findDriverByBlackList(b.getId()).equals(i))
                  driverList.remove(i);
           }
        List<String> list=new LinkedList<String>();
        if(order.getType()==1)     //快车
         for(Integer i:driverList)
         {
             if(driverDataAccess.getByID(i).getState()==null)
                throw new illegelZhiZhen("Driver State is illegal");
             if(driverDataAccess.getByID(i).getState().equals(1))
                 list.add("d"+i);
          }
        else
            if(order.getType()==2)  //专车
                for(Integer i:driverList)
                {
                    if(driverDataAccess.getByID(i).getState()==null)
                        throw new illegelZhiZhen("Driver State is illegal");
                    if(driverDataAccess.getByID(i).getState().equals(2))
                        list.add("d"+i);
                }
        return list;
        //返回值已按推送需求；订单模块可以直接调用
    }

    //10s刷新为乘客推送周围司机位置
    public boolean pushNearYouDriver(String x, String y,Integer id){
        String pAlias="p"+id;
        String passenerGeoCode=GeohashUtils.encodeLatLon(Double.valueOf(x),Double.valueOf(y),5);
        List<Integer> driverList=locationDao.findByGeoCode(passenerGeoCode);
        List<Location> listLocation=new LinkedList<>();
       // List<Driver> listDriver=new LinkedList<>();
        //System.out.println(passenerGeoCode);
        //System.out.println(GetDistance(14.98,13.0,15.0001,13.0));
        for (Integer i:driverList)
        {
            System.out.println(i);
            System.out.println(locationDao.findById(i).getGeoCode());
        }
        Location locatio=null;
        if(null == driverList || driverList.size() ==0 ){  //乘客周围没有司机
            Push apush = new Push(pAlias,"", "Location", locatio);
            pushDistance.pushAll(apush);
        }

        for (Integer i:driverList)
        {
            Location location=new Location();
            location.setX(locationDao.findById(i).getX());
            location.setY(locationDao.findById(i).getY());
            // System.out.println(location.getX());
            // System.out.println(location.getY());
            listLocation.add(location);
         //   listDriver.add(driverDataAccess.getByID(i));
        }
        Push push = new Push(pAlias, "", "List<Location>", listLocation);
        pushDistance.pushAll(push);
        return true;
    }

    //接单后推送向乘客推送司机位置（订单id）
    public boolean pushYourDriverSite(Integer id){
        Order order=orderDataAccess.getByID(id);
        Passenger passenger=orderDataAccess.findPassengerByOrder(id);
        Driver driver=orderDataAccess.findDriverByOrder(id);
        if(order==null)
            throw new NoOrderException("orderNotFound in service-serviceLocation");
        String pAlias="p"+passenger.getId();
        Integer driverId=driver.getId();
        Location location=new Location();
        location.setX(locationDao.findById(driverId).getX());
        location.setY(locationDao.findById(driverId).getY());
       // System.out.println(location.getX());
        Push apush = new Push(pAlias,"","Location", location);
       if(pushDistance.pushAll(apush))
            return true;
       return false;
    }

  /*  public static Map getAreaLongAndDimen(String addr) {
        try {
            addr = new String(addr.getBytes("UTF-8"), "GBK");
        }//因为高德地图用的是linux系统，所以其使用的是gbk的编码，所以在这里你用的是utf-8的话，就得转换成gbk的编码格式。
        catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        String str = "http://restapi.amap.com/v3/geocode/geo?address=" + addr + "&output=JSON&key=81a0fe26dea44894776bd485a934a1de";//key值
        HashMap param = new HashMap();
     //   param.put("info", "erro");
        InputStream inputStream = null;
        try {
            URL url = new URL(str);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setConnectTimeout(5 * 1000);//超时时间
            urlConnection.setRequestProperty("contentType", "utf-8");//字符集
            urlConnection.connect();
            inputStream = urlConnection.getInputStream();
            JsonNode jsonNode = new ObjectMapper().readTree(inputStream);//jackson		   
            if (jsonNode.findValue("status").textValue().equals("1")&& jsonNode.findValue("geocodes").size() > 0){
                String[] degree = jsonNode.findValue("geocodes").findValue("serviceLocation").textValue().split(",");
                param.put("longitude", degree[0]);
                param.put("dimension", degree[1]);
                param.put("info", "success");
            }
        } catch (MalformedURLException e) {
            param.put("info", "erro");
        } catch (IOException e) {
            param.put("info", "erro");
        } finally {
            try {
                if (null != inputStream) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return param;
    }
    */
    //更新理想位置
    /*public boolean updateIdealSite(String idealDepartSite,String idealDestinationSite,Integer orderId){
        Order order=orderDataAccess.getByID(orderId);
        try {
            order.setIdealDepartSite(idealDepartSite);
            order.setIdealDestinationSite(idealDestinationSite);
            orderDataAccess.updateOrder(order);
        }catch (Exception e){
            e.getMessage();
            return  false;
        }
        return true;
    }

    //保存实际出发位置
    public boolean updateActualDepartSite(String actualDepartSite,Integer orderId){
        Order order=orderDataAccess.getByID(orderId);
        try{
            order.setActualDepartSite(actualDepartSite);
            orderDataAccess.updateOrder(order);
        }catch (Exception e)
        {
            e.getMessage();
            return false;
        }
        return true;
    }

    //保存实际到达位置
    public boolean updateActualDestinationSite(String actualDestinationSite,Integer orderId){
        Order order=orderDataAccess.getByID(orderId);
        try{
            order.setActualDestinationSite(actualDestinationSite);
            orderDataAccess.updateOrder(order);
        }catch (Exception e)
        {
            e.getMessage();
            return false;
        }
        return true;
    }

    //根据地址返回经纬度


    //根据经纬度得到地址
    public static String getAdd(String lat,String log){
        //lat 小  log  大
        //参数解释: 纬度,经度 type 001 (100代表道路，010代表POI，001代表门址，111可以同时显示前三项)
        String urlString = "http://gc.ditu.aliyun.com/regeocoding?l="+lat+","+log+"&type=010";
        String res = "";
        try {
            URL url = new URL(urlString);
            java.net.HttpURLConnection conn = (java.net.HttpURLConnection)url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            java.io.BufferedReader in =
                    new java.io.BufferedReader(new java.io.InputStreamReader(conn.getInputStream(),"UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                res += line+"\n";
            }
            in.close();
        } catch (Exception e) {
            System.out.println("error in wapaction,and e is " + e.getMessage());
        }
        System.out.println(res);
        return res;
    }
     */
}
