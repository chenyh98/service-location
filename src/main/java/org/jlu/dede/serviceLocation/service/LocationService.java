package org.jlu.dede.serviceLocation.service;


import java.util.List;

public interface LocationService {
    //保存乘客地理位置
   public boolean storePassengerLocation(String latitude, String longitude, Integer id);
   // 保存司机地理位置
   public boolean storeDriverLocation(String latitude, String longitude, Integer id);
    //计算两次间隔之间的实际里程,并推送给司机和乘客
   public  boolean calculateDistance(String latitude, String longitude, Integer id);
   //寻找乘客发起订单要推送司机的List（接车类型）
   public List<String> findDriver(String x, String y, Integer id);
   //10s刷新为乘客推送周围司机位置
    public boolean pushNearYouDriver(String x, String y, Integer id);
    //接单后推送向乘客推送司机位置（订单id）
    public boolean pushYourDriverSite(Integer id);


    //保存理想出发位置和实际位置
  //  public  boolean updateIdealSite(String idealDepartSite,String idealDestinationSite,Integer id);
    //保存实际出发位置
  //  public boolean updateActualDepartSite(String actualDepartSite,Integer id);
    //保存实际到达位置
   // public boolean updateActualDestinationSite(String actualDepartSite,Integer id);
}
