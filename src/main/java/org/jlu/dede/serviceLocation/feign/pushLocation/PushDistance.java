package org.jlu.dede.serviceLocation.feign.pushLocation;


import org.jlu.dede.publicUtlis.model.Push;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "push-model",url="10.100.0.0:8764")
public  interface PushDistance {
    //推送自定义消息：根据alias推给个人
    @PostMapping("/pushOne/pushMessage")
    boolean pushOneMessage(@RequestBody Push apush);

    //推送自定义消息：根据alias推给多个用户
    @PostMapping("/pushMany/pushMessage")
    boolean pushManyMessage(@RequestBody Push apush);

    @PostMapping("/pushAll")
     boolean pushAll(@RequestBody Push apush);

}
