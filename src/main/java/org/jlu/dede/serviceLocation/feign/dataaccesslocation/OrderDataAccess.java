package org.jlu.dede.serviceLocation.feign.dataaccesslocation;


import org.jlu.dede.publicUtlis.model.Driver;
import org.jlu.dede.publicUtlis.model.Order;
import org.jlu.dede.publicUtlis.model.Passenger;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "data-access",url="10.100.0.2:8765")//,fallback = OrderDataAccessHystric.class)   //@ FeignClient（“服务名”），来指定调用哪个服务
public interface OrderDataAccess {
    //更改订单
    @PostMapping(value = "/orders/{id}")
    public void updateOrder(@PathVariable Integer id, @RequestBody Order order);

    //查询订单
   // @GetMapping(value = "/orders/{id}")
    @RequestMapping(value = "/orders/{id}",method = RequestMethod.GET)
    public Order getByID(@PathVariable("id") Integer id);

    @GetMapping("/passengers/order/{id}")
    public Passenger findPassengerByOrder(@PathVariable("id") Integer id);

    @GetMapping("/drivers/order/{id}")
    public Driver findDriverByOrder(@PathVariable("id") Integer id);

    @GetMapping("/passengers/blacklist/{id}")
    public Passenger findPassengerByBlackList(@PathVariable Integer id);

    @GetMapping("/drivers/blacklist/{id}")
    public Driver findDriverByBlackList(@PathVariable Integer id);


}
