package org.jlu.dede.serviceLocation.feign.dataaccesslocation;


import org.jlu.dede.publicUtlis.model.BlackList;
import org.jlu.dede.publicUtlis.model.Passenger;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "data-access",url="10.100.0.2:8765")//,fallback = PassengerDataAccessHystric.class)    //@ FeignClient（“服务名”），来指定调用哪个服务
public interface PassengerDataAccess {
    //查看乘客信息
  //  @GetMapping("")
    @RequestMapping(value = "/passengers/{id}",method = RequestMethod.GET)
    public Passenger getByID(@PathVariable("id") Integer id);

    //修改乘客信息
    @PostMapping("/passengers")
    public void updatePassenger(@RequestBody Passenger passenger);

    @RequestMapping(value = "/passengers",method = RequestMethod.POST)
    //dataaccess中增加乘客的接口
    public void addPassenger(@RequestBody Passenger passenger);

    //得到乘客黑名单
    @GetMapping("/blacklists/passengers/{passenger_id}")
    public List<BlackList> getByPassenger(@PathVariable Integer passenger_id);

}
