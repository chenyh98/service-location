package org.jlu.dede.serviceLocation.feign.dataaccesslocation;


import org.jlu.dede.publicUtlis.model.Driver;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "data-access",url="10.100.0.2:8765")//,fallback = DriverDataAccessHystric.class)  //@ FeignClient（“服务名”），来指定调用哪个服务
public interface DriverDataAccess {
    //修改司机信息
    @PostMapping(value = "/drivers")
    public void updateDriver(@RequestBody Driver driver);

    //查某司机信息
   // @GetMapping(value="/drivers/{id}")
    @RequestMapping(value = "/drivers/{id}",method = RequestMethod.GET)
    public Driver getByID(@PathVariable("id") Integer id);

    //实时位置


}
