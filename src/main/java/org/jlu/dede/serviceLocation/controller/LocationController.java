package org.jlu.dede.serviceLocation.controller;


import org.jlu.dede.serviceLocation.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/location")
public class LocationController {
    @Autowired
    private LocationService locationService;

    //传递司机id,两个数据库表统一
    @RequestMapping(value = "/getDriverId/{id}",method = RequestMethod.POST)
    public boolean getDriverId(@PathVariable Integer id) {
        return locationService.storeDriverLocation(null,null,id);
    }

    //
//    @RequestMapping(value = "/updatePassengerLocation",method = RequestMethod.POST)
//    public boolean updateLocationPassenger(String x,String y,Integer id){
//        return locationService.storePassengerLocation(x,y,id);
//    }

    //更新司机位置(my mySQL)
    @RequestMapping(value = "/updateDriverLocation",method = RequestMethod.POST)
    public boolean updateLocationDriver(String x,String y,Integer id){
        return  locationService.storeDriverLocation(x,y,id);
}

    //计算行驶距离，推送给司机和乘客  ???推送
    @RequestMapping(value = "/updateDistance",method = RequestMethod.POST)
    public boolean updateDistance(String x,String y,Integer id) {
        return locationService.calculateDistance(x,y,id);
    }

   //寻找要推送的司机（乘客下单）,给司机推送订单  (订单模块调用)//乘客经纬度，订单id
    @RequestMapping(value = "/findIdealGoSiteDriver",method = RequestMethod.POST)
    public List<String> findIdealGoSiteDriver(String x,String y,Integer id) {
        return locationService.findDriver(x,y,id);
    }

    //；10s刷新为乘客推送周围司机位置        ???推送
    @RequestMapping(value = "/findNowSiteDriver",method = RequestMethod.POST)
    public boolean findNowSiteDriver(String x,String y,Integer id) {
        return locationService.pushNearYouDriver(x,y,id);
    }

    //接单后向乘客推送司机位置（订单id） ???推送
    @RequestMapping(value = "/findYourDriverSite",method = RequestMethod.POST)
    public boolean findYourDriverSite(Integer id){
        return locationService.pushYourDriverSite(id);
    }

}
