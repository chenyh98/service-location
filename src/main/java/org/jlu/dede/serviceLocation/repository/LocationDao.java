package org.jlu.dede.serviceLocation.repository;

import org.jlu.dede.serviceLocation.bean.DriverLocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocationDao extends JpaRepository<DriverLocation,Long> {
    DriverLocation findById(Integer id);
    @Query("select id from DriverLocation driverLocation where geoCode like concat(?1 ,'%') ")
    public List <Integer> findByGeoCode(String geoCode);//@Param("geoCode")
}
